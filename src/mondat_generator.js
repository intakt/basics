// elso alkalom random mondat generator

var alanyok = ["macska", "portás", "néni", "torta", "agár", "fenegyerek"]
var igek = ["elment", "felkiáltott", "elsírta magát", "meghatódott", "unatkozott"]
var jelzok = [ "szelíd", "aranyos", "morcos", "álmos", "nagyzoló"]

// egy function ami visszaad egy veletlen elemet abbol a listabol amit beadsz neki
var randomFrom = function(bejovo_lista){
  // Math.floor function lekerekiti a szamokat, ez azert kell mert az index nem lehet tort szam
  // Math.random function visszad egy random szamot 0 es 1 kozott (0 lehet de 1 sosem)
  // ezzel a szammal ha megszorozzuk a lista hosszat es azt lekerekitjuk az biztosan egy letezo index lesz a listaban
  //                                            .length a lista hossza
  var randomIndex = Math.floor( Math.random() * bejovo_lista.length )
  return bejovo_lista[ randomIndex ]
}
// input -> output
var maganhangzovalKezdodik = function(input_szoveg){
  // definialunk egy stringet benne a lehetséges magánhangzókkal
  var maganhangzok = "aáeéiíoóöőuúüű"

  // az stringeken beluli karakterekre hasonloan hivatkozhatunk mint arrayeknel
  // definialunk egy elsobetu nevu stringet amibe beletesszuk az inputkent megkapott szoveg elso karakteret (!! indexek 0-rol indulnak !!)
  var elsobetu = input_szoveg[ 0 ]

  //          minden stringgen meghivhatok bizonyos function-ok
  //          pl az "indexOf" aminek ha beadsz egy stringet visszaadja az elso elofordulasat abban a stringben amin meghivtad, tehat egy szamot ad vissza, ami -1 hogyha nem fordult elo sehol sem a keresett karakter vagy string
  // a mi esetunkben a "maganhangzok" valtozonkban szeretnenk megtalani az elso betut
  var index = maganhangzok.indexOf( elsobetu )
  // a function vegul visszaad egy boolean erteket, ha -1 nel nagyobb (vagyis megtalaltuk az elso betut a maganhangzoink kozott akkor true egyebkent false
  return index > -1

  // ez az egesz function lehetne csak ennyi is akar ;)
  // return "aáeéiíoóöőuúüű".indexOf( input_szoveg[ 0 ] ) > -1
}

var randomMondat = function(){
  // csinalunk harom valtozot mindegyik egy random kivalasztott elem a megfelelo listabol
  var random_jelzo = randomFrom( jelzok )
  var random_alany = randomFrom( alanyok )
  var random_ige = randomFrom( igek )

  // definialunk egy nevelot eloszot csak ures stringkent
  var nevelo = ""
  // attol fuggoen hogy mit ad vissza a maganhangzovalKezdodik functionunk a nevelot atallitjuk a megfelelore
  if( maganhangzovalKezdodik( random_jelzo ) ){
    nevelo = "az"
  }else{
    nevelo = "a"
  }

  // visszadjuk a nevelot es a valasztott elemeket (szokozokkel osszeadva)
  return nevelo + " " + random_jelzo + " " + random_alany + " " + random_ige + "."
}

// kiirunk a consoleba egy randomMondatot amikor betolt az oldal
var elsoRandomMondat = randomMondat()
console.log( elsoRandomMondat )
